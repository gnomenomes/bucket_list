<?php namespace App;


use Illuminate\Database\Eloquent\Model;

class ChecklistRoleUser extends Model
{
    protected $table = 'checklist_role_user';

    protected $fillable = ['checklist_id', 'role_id', 'user_id'];

}
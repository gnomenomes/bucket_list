<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);

        $gate->define('access-as-owner',function ($user,$checklist) {
            return $checklist->owner()->where('id',$user->id)->exists();
        });

        $gate->define('access-as-editor',function ($user, $checklist) {
            return $checklist->editors()->where('id',$user->id)->exists();
        });

        $gate->define('access-as-reader',function ($user, $checklist) {
           return $checklist->readers()->where('id',$user->id)->exists();
        });

        $gate->define('access',function ($user, $checklist) {
           return $checklist->users()->where('id',$user->id)->exists();
        });
    }
}

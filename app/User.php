<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Role;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function checklists()
    {
        return $this->belongsToMany('App\Checklist', 'checklist_user');
    }

    public function checklistsOwned()
    {
        return $this->belongsToMany('App\Checklist', 'checklist_user')
            ->where("role_id", "=",Role::where("name","=","owner")->first()->id);
    }

    public function checklistsCanEdit()
    {
        return $this->belongsToMany('App\Checklist', 'checklist_user')
            ->where("role_id", "=",Role::where("name","=","editor")->first()->id);
    }

    public function checklistsCanOnlyRead()
    {
        return $this->belongsToMany('App\Checklist', 'checklist_user')
            ->where("role_id", "=",Role::where("name","=","reader")->first()->id);
    }
}

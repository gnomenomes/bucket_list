<?php namespace App;


use Illuminate\Database\Eloquent\Model;


class Checklist extends Model
{
    protected $table = 'checklists';

    protected $fillable = ['name', 'summary', 'priority'];

    protected $dates = ['created_at', 'updated_at'];

    public function tasks()
    {
        return $this->hasMany('App\Task');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function scopeOwned($query)
    {
        $ownerRole = Role::where('name','owner')->first();
        return $query->where("role_id",'=',$ownerRole->id);
    }

    public function scopeEditable($query)
    {
        $editorRole = Role::where('name','editor')->first();
        return $query->where("role_id",'=',$editorRole->id);
    }

    public function scopeReadable($query)
    {
        $readerRole = Role::where('name','reader')->first();
        return $query->where("role_id",'=',$readerRole->id);
    }

    public function owner()
    {
        $roleId = Role::where('name','=', 'owner')->first()->id;
        return $this->belongsToMany('App\User')
            ->where('role_id',$roleId);
    }

    public function editors()
    {
        $roleId = Role::where('name','=', 'editor')->first()->id;
        return $this->belongsToMany('App\User')
            ->where('role_id',$roleId);
    }

    public function readers()
    {
        $roleId = Role::where('name','=', 'reader')->first()->id;
        return $this->belongsToMany('App\User')
            ->where('role_id',$roleId);
    }

}
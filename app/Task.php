<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';

    protected $fillable = ['name', 'checklist_id', 'details', 'priority'];

    protected $dates = ['created_at', 'updated_at'];

    public function checklist()
    {
        return $this->belongsTo('App\Checklist');
    }
}
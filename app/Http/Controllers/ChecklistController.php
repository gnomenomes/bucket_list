<?php namespace App\Http\Controllers;

use App\Http\Requests\ChecklistRequest;
use Illuminate\Http\Request;
use App\Checklist;
use App\Role;

class ChecklistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $checklistsOwned = \Auth::user()->checklistsOwned;
        $checklistsEditable = \Auth::user()->checklistsCanEdit;
        $checklistsReadable = \Auth::user()->checklistsCanOnlyRead;
        return view('checklists.index', compact('checklistsOwned','checklistsEditable','checklistsReadable'));
    }

    public function show($id)
    {
        $checklist = Checklist::findOrFail($id);

        if (\Auth::user()->cannot('access', $checklist)) {
            return redirect('checklists');
        }
        $tasks = $checklist->tasks;
        return view('checklists.show', compact('checklist', 'tasks'));
    }

    public function create()
    {
        $user = \Auth::user();
        return view('checklists.create', compact('user'));
    }

    public function store(ChecklistRequest $request)
    {
        $checklist = Checklist::create($request->all());
        $checklist->users()->sync([\Auth::user()->id => ['role_id' => Role::where("name","=","owner")->first()->id]]);
        return redirect('/checklists');
    }

    public function destroy($id)
    {
        $checklist = Checklist::findOrFail($id);
        if (\Auth::user()->cannot('access-as-owner', $checklist)) {
            return redirect('checklists');
        }
        $checklist->delete();
        return redirect('/checklists');
    }

    public function edit($id)
    {
        $checklist = Checklist::findOrFail($id);
        $tasks = $checklist->tasks;
        if (\Auth::user()->cannot('access-as-owner',$checklist) && \Auth::user()->cannot('access-as-editor', $checklist)) {
            return redirect('checklists');
        }
        return view('checklists.edit', compact('checklist', 'tasks'));
    }

    public function update($id, ChecklistRequest $request)
    {
        $checklist = Checklist::findOrFail($id);
        if (\Auth::user()->cannot('access-as-owner',$checklist) && \Auth::user()->cannot('access-as-editor', $checklist)) {
            return redirect('checklists');
        }
        $checklist->update($request->all());
        return redirect('/checklists');
    }

}
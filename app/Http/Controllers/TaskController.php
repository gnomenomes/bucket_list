<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Checklist;

class TaskController extends Controller
{
    public function create($checklistID)
    {
        $checklist = Checklist::findOrFail($checklistID);
        $tasks = $checklist->tasks;
        return view('tasks.create', compact('checklist', 'tasks'));
    }

    public function store(Request $request)
    {
        $requirements = $request->only('name', 'details','priority');
        foreach($requirements as $req)
        {
            if ($req == null)
            {
                return redirect('checklists/'.$request->checklist_id.'/tasks/create')
                    ->withErrors('Please enter information into all required fields');
            }
        }
        $task = Task::create($request->all());
        return redirect()->route('checklists.show', [$task->checklist_id]);
    }

    public function update(Request $request)
    {
        $task = Task::find($request->id);
        $task->update($request->all());
        return redirect()->route('checklists.show', [$task->checklist_id]);
    }

    public function destroy($id)
    {
        $checklist = Checklist::find($id);
        if (\Auth::user()->cannot('access-as-owner', $checklist)) {
            return redirect('checklists');
        }
        $checklist->delete();
        return redirect('/checklists');
    }

    public function edit($listId, $taskId)
    {
        $task = Task::findOrFail($taskId);
        $checklist = Checklist::findOrFail($listId);
        if (\Auth::user()->cannot('access-as-owner',$checklist) && \Auth::user()->cannot('access-as-editor', $checklist)) {
            return redirect('checklists');
        }
        return view('tasks.edit', compact('task', 'checklist'));
    }
}
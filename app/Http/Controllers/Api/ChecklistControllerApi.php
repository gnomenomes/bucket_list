<?php namespace App\Http\Controllers\Api;

use App\Checklist;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChecklistRequest;
use Illuminate\Http\Request;

class ChecklistControllerApi extends Controller
{
    public function show($id)
    {
        $checklist = Checklist::find($id);
        $tasks = $checklist->tasks;
        return view('checklists._show', compact('checklist', 'tasks'));
    }

}
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Main
Route::get('/', function () {
    return view('layouts/home');
});

//Authentication
Route::get('auth/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);
Route::controllers(['auth' => 'Auth\AuthController']);
//Route::post('auth/login', 'Auth\AuthController@postLogin');

// Checklists and Tasks
Route::resource('/checklists', 'ChecklistController');
Route::resource('checklists.tasks', 'TaskController');

//Api
Route::group(['prefix' => 'api', 'namespace' => 'Api', 'middleware' => 'auth'], function() {
    Route::resource('checklists', 'ChecklistControllerApi');
});


<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ChecklistRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'summary' => 'required',
            'priority' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name' => 'Your name is required',
            'summary' => 'A summary is required',
            'priority' => 'The priority is required'
        ];
    }
}

var aboutTxt =  "This site allows you to create and manage checklists. " +
    "You can create and prioritize tasks within a checklist that may be shared with friends."
var toggle = false;

function showAbout() {
    if (!toggle) {
        document.getElementById('aboutText').innerHTML = aboutTxt;
    } else {
        document.getElementById('aboutText').innerHTML = "";
    }
    toggle = !toggle;
}


// Define stuff when DOM is ready
document.addEventListener("DOMContentLoaded", function(event) {
    var checklistContainer = document.querySelector('div.checklistTables');
    var checklistHideButton = document.getElementById('checklistViewHide');
    checklistContainer.addEventListener("click", function(event) {
        if(event.target && event.target.className === 'viewChecklist')
        {
            viewChecklist(event.target);
        }
    });
    checklistHideButton.addEventListener("click", function(event) {
        console.log("here");
        hideChecklist();
    });
});

function viewChecklist(button) {
    var checklistId = button.dataset.checklistid;
    getChecklistFromApi(checklistId);
}

function getChecklistFromApi(checklistId)
{
    var xhttp = new XMLHttpRequest();
    var url = 'api/checklists/';
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            displayChecklist(xhttp.responseText);
        }
    };
    xhttp.open("GET", url + checklistId, true);
    xhttp.send();
}

function displayChecklist(htmlResponse)
{
    var checklistDisplay = document.getElementById('checklistView');
    var checklistHideButton = document.getElementById('checklistViewHide');
    checklistDisplay.innerHTML = htmlResponse;
    checklistDisplay.style.display = 'block';
    checklistHideButton.style.display = 'block';
}

function hideChecklist()
{
    var checklistDisplay = document.getElementById('checklistView');
    var checklistHideButton = document.getElementById('checklistViewHide');
    checklistDisplay.style.display = "none";
    checklistHideButton.style.display = 'none';
}


function showError()
{
    var checklistDisplay = document.getElementById('checklistView');
    var checklistHideButton = document.getElementById('checklistViewHide');
    checklistDisplay.innerHTML = "There was an error with the request";
    checklistDisplay.style.display = 'block';
    checklistHideButton.style.display = 'block';
}
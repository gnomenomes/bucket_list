document.addEventListener("DOMContentLoaded", function(event) {
    var formContainer = document.querySelector("#formContainer");
    formContainer.addEventListener("click", function(event) {
        if(event.target && event.target.name === 'formEntryRevert')
        {
            revertEntry(event.target);
        }
    });
});

function revertEntry(button)
{
    var type = button.dataset.type
    var divObj = document.getElementById(type + "Div");
    var content = divObj.innerHTML;
    divObj.innerHTML = content;
    return;
}

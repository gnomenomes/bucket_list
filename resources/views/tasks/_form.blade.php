@extends('layouts.app')

<script src="/js/formEdit.js"></script>

<div class="form-group">
    {!! Form::hidden('checklist_id', $checklist->id) !!}
</div>

<div id="formContainer" class="form-horizontal">
    <div id="nameDiv" class="form-group">
        {!! Form::label('name', 'Name:') !!}
        <div class="row">
            <div class="col-xs-5">
                <p>{{$task->name}}</p>
            </div>
            <div class="col-xs-5">
                {!! Form::text('name', null, ['class'=>'form-control stick-to-right']) !!}
            </div>
            <div class="col-xs-2">
                <input type="button" class="padded-left" data-type="name" name="formEntryRevert" value="Revert">
            </div>
        </div>
    </div>

    <div id="detailsDiv" class="form-group">
        {!! Form::label('details', 'Details:') !!}
        <div class="row">
            <div class="col-xs-5">
                <p>{{$task->details}}</p>
            </div>
            <div class="col-xs-5">
                {!! Form::textarea('details', null, ['class'=>'form-control']) !!}
            </div>
            <div class="col-xs-2">
                <input type="button" class="padded-left" data-type="details" name="formEntryRevert" value="Revert">
            </div>
        </div>
    </div>

    <div id="priorityDiv" class="form-group">
        {!! Form::label('priority', 'Priority:') !!}
        <div class="row">
            <div class="col-xs-5">
                <p>{{$task->priority}}</p>
            </div>
            <div class="col-xs-5">
                {!! Form::number('priority', null, ['class'=>'form-control']) !!}
            </div>
            <div class="col-xs-2">
                <input type="button" class="padded-left" data-type="priority" name="formEntryRevert" value="Revert">
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::submit($submitButtonName, ['class'=>'btn btn-primary form-control']) !!}
</div>

<div class="form-group">
    <a class="btn btn-default form-control" href="{{ url('/checklists/'.$checklist->id) }}">
        Back to Tasks
    </a>
</div>

<!--
<div class="form-group"> {{--
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('details', 'Details:') !!}
    {!! Form::textarea('details', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('priority', 'Priority:') !!}
    {!! Form::number('priority', null) !!}
</div>  --}}  -->


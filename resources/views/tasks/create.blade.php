@extends('layouts.app')

@section('content')
    <h1>Create Task for Checklist</h1>

    <h2>Checklist Name:</h2>
    {{ $checklist->name }}

    <h2>Task</h2>
    {!! Form::open(['route' => 'checklists.tasks.store']) !!}

    {!! Form::hidden('checklist_id', $checklist->id) !!}

    @include("tasks._form",["submitButtonName" => "Add Task"]);

    {!! Form::close() !!}
@endsection
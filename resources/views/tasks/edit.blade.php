@extends('layouts.app')

@section('content')
    <div>
        <h2>Checklist Name:<br />{{ $checklist->name }}</h2>
        <a href="{{ route('checklists.index') }}"><input type="button" value="Back to checklists"></a>
    </div>

    {!! Form::model($task, ['method' => 'PUT', 'route' => ['checklists.tasks.update', $checklist->id, $task->id]]) !!}

    {!! Form::hidden('id', $task->id) !!}

    @include("tasks._form",["submitButtonName" => "Update Task"]);

    {!! Form::close() !!}
@endsection
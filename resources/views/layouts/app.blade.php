<?php

    $user = \Auth::user();
    $greeting = 'Hello, ';
    if ($user != null)
    {
        $greeting .= $user->username;
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bucket List</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

    <style>
        .navbar-custom {
            color: #FFFFFF;
            margin-top: 5px;
            margin-right: 15px;
            font-size: 20px;
        }
        body {
            padding-top: 50px;
        }
        .stick-to-right {
             float: right;
         }
        .padded-top {
            margin-top: 100px;
        }
        .padded-left {
            margin-left: 30px;
        }
    </style>
</head>
<body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">Bucket List</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                @if($user == null)
                    <form class="navbar-form navbar-right" method="POST" action="{{ url('/auth/login') }}">
                        @include('auth._form');
                    </form>
                @else
                    {!! Form::open(['route' => 'logout', 'method' => 'get']) !!}

                    <div class="navbar-form navbar-right">
                        <ul class="nav navbar-nav navbar-custom">
                            {{ $greeting }}
                        </ul>
                        {!! Form::submit('Logout', ['class'=>'btn btn-warning']) !!}
                    </div>

                    {!! Form::close() !!}
                @endif

            </div>
        </div>
    </nav>

    <div class="container">
        @include('_messages')
        @yield('content')
    </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

</body>
</html>
@extends('layouts.app')

@section('content')
    <script src="js/home.js"></script>


    <h1>Home Page</h1>
    <div class="jumbotron">
        <div class="container">
            <h1>Hello.</h1>
            <p>This is the place to go to organize your to-do lists.</p>
            <a class="btn btn-success" href="{{ url('checklists') }}">To Checklists</a>
        </div>
    </div>

    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-4">
                <button onclick="showAbout()">About</button>
                <p id="aboutText"><script>aboutTxt</script></p>
            </div>
        </div>
    </div>

<!--
    <div class="container">
        <h2>Public Lists</h2>
        <p>These are some global lists available to everyone:</p>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Checklist</th>
                <th>Updated</th>
                <th>Completed</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Shopping</td>
                <td>Yesterday</td>
                <td>No</td>
            </tr>
        <!-- {{-- @if($items->isEmpty())
                <p>No items.</p>
            @endif
            @foreach($items as $item)
                <tr>
                    <td>{{ $item->name }}
                        - <a href="{{ route('checklists.items.edit', [$checklist->id, $item->id]) }}">Edit</a>
                    </td>
                </tr>
            @endforeach --}} -->
            </tbody>
        </table>
    </div>

@stop
<script src="/js/formEdit.js"></script>

<div id="formContainer" class="form-horizontal">
    <div id="nameDiv" class="form-group">
        {!! Form::label('name', 'Name:') !!}
        <div class="row">
            <div class="col-xs-5">
                <p>{{$checklist->name}}</p>
            </div>
            <div class="col-xs-5">
                {!! Form::text('name', null, ['class'=>'form-control stick-to-right']) !!}
            </div>
            <div class="col-xs-2">
                <input type="button" class="padded-left" data-type="name" name="formEntryRevert" value="Revert">
            </div>
        </div>
    </div>

    <div id="summaryDiv" class="form-group">
        {!! Form::label('summary', 'Summary:') !!}
        <div class="row">
            <div class="col-xs-5">
                <p>{{$checklist->summary}}</p>
            </div>
            <div class="col-xs-5">
                {!! Form::textarea('summary', null, ['class'=>'form-control']) !!}
            </div>
            <div class="col-xs-2">
                <input type="button" class="padded-left" data-type="summary" name="formEntryRevert" value="Revert">
            </div>
        </div>
    </div>

    <div id="priorityDiv" class="form-group">
        {!! Form::label('priority', 'Priority:') !!}
        <div class="row">
            <div class="col-xs-5">
                <p>{{$checklist->priority}}</p>
            </div>
            <div class="col-xs-5">
                {!! Form::number('priority', null, ['class'=>'form-control']) !!}
            </div>
            <div class="col-xs-2">
                <input type="button" class="padded-left" data-type="priority" name="formEntryRevert" value="Revert">
            </div>
        </div>
    </div>

    <div class="form-group">
        {!! Form::submit($submitButtonName, ['class'=>'btn btn-primary form-control']) !!}
    </div>

    <div class="form-group">
        <a class="btn btn-default form-control" href="{{ route('checklists.index') }}">
            Back to checklists
        </a>
    </div>
</div>

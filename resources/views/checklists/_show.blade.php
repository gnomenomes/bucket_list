<style>
    a, a:hover, a:visited, a:active {
        color:inherit;

    }
</style>

<h2>Checklist: {{$checklist->name}}</h2>
<ul>
    <li><strong>Summary:</strong> {{ $checklist->summary }}</li>
</ul>

@if($tasks->isEmpty())
    <p>No tasks.</p>
@endif

<table class="table table-striped">
    <thead>
    <tr>
        <th>Name</th>
        <th>Details</th>
        <th>Priority</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($tasks as $task)
        <tr>
            <td>{{ $task->name }}</td>
            <td>{{ $task->details }}</td>
            <td>{{ $task->priority }}</td>
            <td>
                <a href="{{ route('checklists.tasks.edit', [$checklist->id, $task->id]) }}" value="Edit"><input type="button" value="Edit"></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
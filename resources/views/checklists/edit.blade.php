@extends('layouts.app')

@section('content')
    <style>
        a, a:hover, a:visited, a:active {
            color:inherit;

        }
    </style>

    <h1>Checklist</h1>
    {!! Form::model($checklist, ['method' => 'PUT', 'route' => ['checklists.update', $checklist->id]]) !!}

    @include('checklists._form',["checklist" => $checklist, "submitButtonName" => "Update Checklist"])

    {!! Form::close() !!}

    <h3>Tasks</h3>

    <a href="{{ route('checklists.tasks.create', $checklist->id) }}">Add an task</a>

    @if($tasks->isEmpty())
        <p>No tasks.</p>
    @else
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Details</th>
                <th>Priority</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($tasks as $task)
                <tr>
                    <td>{{ $task->name }}</td>
                    <td>{{ $task->details }}</td>
                    <td>{{ $task->priority }}</td>
                    <td>
                        <a href="{{ route('checklists.tasks.edit', [$checklist->id, $task->id]) }}"><input type="button" value="Edit"></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection
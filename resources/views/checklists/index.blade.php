@extends('layouts.app')

@section('content')
    <script src="js/checklistView.js"></script>
    <style>
        a, a:hover, a:visited, a:active {
            color:inherit;

        }
    </style>

    <h2>Checklists</h2>
    <p><a href="{{ url('/checklists/create') }}">Add a Checklist</a></p>

    @if ($checklistsEditable->count() == 0 && $checklistsReadable->count() == 0)
        <p>There are no checklists in the system.</p>
    @else
        <div class="checklistTables">
        <table class="table table-striped">
            <h4>Checklists you own</h4>
            <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($checklistsOwned as $checklist)
                <tr>
                    <td>{{ $checklist->name }}</td>
                    <td>{{ $checklist->summary }}</td>
                    <td>
                        <input type="button" data-checklistId="{{$checklist->id}}" class="viewChecklist" value="View"> -
                        <a  href="{{ route('checklists.edit', $checklist->id) }}"><input type="button" value="Edit"></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <table class="table table-striped">
            <thead>
            <h4>Others' checklists you can edit</h4>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($checklistsEditable as $checklist)
                <tr>
                    <td>{{ $checklist->name }}</td>
                    <td>{{ $checklist->summary }}</td>
                    <td>
                        <input type="button" data-checklistId="{{$checklist->id}}" class="viewChecklist" value="View"> -
                        <a  href="{{ route('checklists.edit', $checklist->id) }}"><input type="button" value="Edit"></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <table class="table table-striped">
            <thead>
            <h4>Checklists you can only view</h4>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($checklistsReadable as $checklist)
                <tr>
                    <td>{{ $checklist->name }}</td>
                    <td>{{ $checklist->summary }}</td>
                    <td>
                        <input type="button" data-checklistId="{{$checklist->id}}" class="viewChecklist" value="View Only">
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    @endif

    <div class="padded-top">
        <input id="checklistViewHide" class="stick-to-right" style="display:none" type="button" value="Hide">
        <div id="checklistView" style="display:none">

        </div>
    </div>

@endsection
@extends('layouts.app')

@section('content')
    <h1>Checklist</h1>
    {!! Form::open(['route' => 'checklists.store']) !!}

    @include('checklists._form',["submitButtonName" => "Add Checklist"]);

    {!! Form::close() !!}
@endsection
@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Login</div>
        <div class="panel-body">
            <form role="form" method="POST" action="{{ url('/auth/login') }}">
                @include('auth._form')
            </form>
        </div>
    </div>
@endsection
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="form-group">
    <input type="text" name="username" placeholder="Username" class="form-control" value="{{ old('username') }}">
</div>
<div class="form-group">
    <input type="password" name="password" placeholder="Password" class="form-control">
</div>
<div class="form-group">
    <button type="submit" class="btn btn-primary">Login</button>
</div>
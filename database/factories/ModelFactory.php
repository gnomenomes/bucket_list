<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'username' => $faker->userName,
        'email' => $faker->email,
        'password' => bcrypt('password'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Checklist::class, function (Faker\Generator $faker) {
    $listNames = [
        'Random List',
        'Paranoid List',
        'Rational List',
        'Boring List',
        'Creative List',
    ];

    return [
        'name' => $listNames[$faker->numberBetween(0,count($listNames) - 1)],
        'summary' => $faker->sentence(7),
        'priority' => $faker->numberBetween(1,3),
        'created_at' => Carbon\Carbon::now(),
    ];
});

$factory->define(App\Task::class, function (Faker\Generator $faker) {
    $taskNames = [
        'Shop',
        'Study',
        'Work',
        'Exercise',
        'Have Fun',
    ];

    return [
        'checklist_id' => $faker->numberBetween(1,count(App\Checklist::all()->toArray())),
        'name' => $taskNames[$faker->numberBetween(0,count($taskNames) - 1)],
        'details' => $faker->sentence(7),
        'priority' => $faker->numberBetween(1,5),
        'completed' => $faker->boolean(25),
        'created_at' => Carbon\Carbon::now(),
    ];
});

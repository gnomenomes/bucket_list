<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Factory as Faker;

class ChecklistUserSeeder extends Seeder {
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('checklist_user')->truncate();

        $faker = Faker::create();
        $checklists = App\Checklist::all();

        foreach ($checklists as $checklist)
        {
            $faker->unique($reset = true);
            $numUsers = $faker->numberBetween(2,3);
            $userIds = [];
            $roleIds = [1];
            for ($i = 0; $i < $numUsers; $i++)
            {
                $userIds[] = $faker->unique()->numberBetween(1, App\User::count());
            }

            DB::table('checklist_user')->insert([
                'checklist_id' => $checklist->id,
                'user_id' => $userIds[0],
                'role_id' => $roleIds[0],
            ]);
            $checklist->users()->sync([$userIds[0]]);

            for ($i = 1; $i < $numUsers; $i++)
            {
                $roleIds[] = $faker->numberBetween(2,3);

                DB::table('checklist_user')->insert([
                    'checklist_id' => $checklist->id,
                    'user_id' => $userIds[$i],
                    'role_id' => $roleIds[$i],
                ]);
                $checklist->users()->sync([$userIds[$i]], false);

            }
        }

    }
}
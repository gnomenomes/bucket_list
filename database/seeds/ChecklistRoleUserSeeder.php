<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Factory as Faker;

class ChecklistRoleUserSeeder extends Seeder {
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('checklist_user')->truncate();

        $faker = Faker::create();
        $checklists = App\Checklist::all();

        foreach ($checklists as $checklist)
        {
            $numUsers = $faker->numberBetween(1,3);
            $userIds = [$faker->numberBetween(1, App\User::count())];
            $roleIds = [1];
            ChecklistRoleUser::create([
                'checklist_id' => $checklist->id,
                'user_id' => $userIds[0],
                'role_id' => $roleIds[0],
            ]);

            for ($i = 1; $i < count($numUsers); $i++)
            {
                $id = $faker->numberBetween(1, App\User::count());
                if (!in_array($id, $userIds))
                {
                    $userIds[] = $id;
                    $roleIds[] = numberBetween(2,App\Role::count());
                    ChecklistRoleUser::create([
                        'checklist_id' => $checklist->id,
                        'user_id' => $userIds[$i],
                        'role_id' => $roleIds[$i],
                    ]);
                }
            }
            //$checklist->users()->sync($userIds);
        }
    }
}
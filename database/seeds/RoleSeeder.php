<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Factory as Faker;
use App\Role;

class RoleSeeder extends Seeder {
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('roles')->truncate();

        $roles = [
            'owner',
            'editor',
            'reader',
        ];

        foreach($roles as $role)
        {
            Role::create([
                'name' => $role
            ]);
        }
    }
}
<?php


use Illuminate\Support\Collection;

class CheckListControllerTest extends TestCase
{
    public function signInAsUser()
    {
        $user = App\User::all()->first();
        $this->visit('checklists')
            ->type($user->username, "username")
            ->type("password", "password")
            ->press("Login")
            ->seePageIs('checklists');
    }

    public function signOut()
    {
        $this->visit('auth/logout');
    }

    public function testUserNeedsToBeLoggedInToAccessChecklists()
    {
        $this->signOut();
        $this->visit('checklists')
             ->seePageIs('auth/login');
    }

    public function testUserNeedsRequiredInputToCreateChecklist()
    {
        $this->signOut();
        $this->signInAsUser();

        $this->visit('checklists/create')
            ->press('Add Checklist')
            ->seePageIs('checklists/create');
    }

    public function testUserCannotEditRandomChecklists()
    {
        $this->signOut();
        $this->signInAsUser();
        $checklistUnowned = 0;
        $checklists = App\Checklist::all()->take(20);
        foreach ($checklists as $checklist) {
            if (!$checklist->users()->lists('id')->contains(\Auth::user()->id))
            {
                $checklistUnowned = $checklist->id;
                break;
            }
        }
        $this->visit('checklists/'.$checklistUnowned)
            ->seePageIs('checklists');
    }
}

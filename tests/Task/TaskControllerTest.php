<?php

class TaskControllerTest extends TestCase
{
    public function signInAsUser()
    {
        $user = App\User::all()->first();
        $this->visit('checklists')
            ->type($user->username, "username")
            ->type("password", "password")
            ->press("Login")
            ->seePageIs('checklists');
    }

    public function signOut()
    {
        $this->visit('auth/logout');
    }

    public function testUserCannotEmptyFieldWhenEditing()
    {
        $this->signInAsUser();
        $checklist = \Auth::user()->checklists->first();
        $task = $checklist->tasks->first();
        $this->visit("/checklists/".$checklist->id."/tasks/".$task->id."/edit");
    }
}